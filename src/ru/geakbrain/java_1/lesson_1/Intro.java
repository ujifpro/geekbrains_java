package ru.geakbrain.java_1.lesson_1;

public class Intro {

    public static void main(String[] args){
        System.out.println("Результат работы task_1: " + task1(11,2,234,47));
        System.out.println("Результат работы task_2: " + task2(12,23));
        System.out.println("Результат работы task_3: " + task3(0));
        task4("Ivan");
        System.out.println(task5(300));
    }

    private static double task1(int a, int b, int c, int d){
        /**
         *  Метод вычисляющий выражение a * (b + (c / d)) и возвращающий результат с плавающей точкой, где a, b, c, d – целочисленные входные параметры этого метода
         *  test
         */
        double result;
        result = a * (b + (c/d));
        return result;
    }

    private static boolean task2(int a, int b){
        /**
         *  Метод, принимающий на вход два целых числа,  и проверяющий что их сумма лежит в пределах от 10 до 20(включительно), если да – вернуть true, в противном случае – false
         */
        boolean result;
        int sum = a+b;
        if(sum >= 10 && sum <=20){
            result = true;
        }
        else{
            result = false;
        }
        return result;
    }

    private static String task3(int a){
        /**
         *  Метод, которому в качестве параметра передается целое число, метод должен проверить положительное ли число передали, или отрицательное.
         */
        String result;
        if(a>=0){
            result = "Введенное число положительное.";
        }
        else{
            result = "Введенное число отрицательное.";
        }
        return result;
    }

    private static void task4(String name){
        /**
         *  Метод, которому в качестве параметра передается строка, обозначающая имя, метод должен вернуть приветственное сообщение «Привет, переданное_имя!»
         */
        System.out.println("Привет, " + name + "!");
    }

    private static String task5(int year){
        /**
         *  Метод, который определяет является ли год високосным.  Каждый 4-й год является високосным, кроме каждого 100-го, при этом каждый 400-й – високосный
         */

        String result = " не високосный год.";

        if(year%400 == 0 || (year%4 == 0 && year%100 !=0)){
            result = " високосный год";
        }

        result = year + result;
        return result;
    }
}
